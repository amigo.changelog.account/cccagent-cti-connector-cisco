## [1.0.24.0] - 2020-01-10

### Changed

* User friendly message with detailed error description has been added when CCCAgent CTI Connector Administration is unable to access configuration (JSON) file. 
* User friendly message with detailed error description has been added when user is unable to start/stop CCCAgent BIS Service through CCCAgent CTI Connector Administration.
* User friendly message with detailed error description has been added when CCCAgent CTI Connector Administration is unable to get running status for CCCAgent BIS Service.
* User friendly message with detailed error description has been added when CCCAgent CTI Connector Administration is unable to update system settings.

### Added
* 0 changes

### Fixed
* 0 changes